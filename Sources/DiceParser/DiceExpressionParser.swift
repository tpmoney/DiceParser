//
//  DiceExpressionParser.swift
//  DiceParser
//
//  Created by  on 2/26/17.
//  Copyright © 2017 Tevis Money. All rights reserved.
//  Licensed under the BSD 3-Clause License, see LICENSE file for details
//

import Foundation

public class DiceExpressionParser {
    
    private let INITIAL_STACK_SIZE = 5   //Initial stack size for parser, shouldn't need to be increased
    
    /*
     *Regular expressions for use in parsing
     */
    private let openParenthesis : NSRegularExpression
    private let closeParenthesis : NSRegularExpression
    private let number : NSRegularExpression
    private let operatorChar : NSRegularExpression
    
    var workStack : Array<String>   //An array for the working stack of operations
    var showDice = false //Whether or not to show the dice rolls in the results
    var showWork = false //Whether or not to show the work steps in the results
    
    public init() {
        openParenthesis = try! NSRegularExpression(pattern: "\\(")
        closeParenthesis = try! NSRegularExpression(pattern: "\\)")
        number = try! NSRegularExpression(pattern: "\\d+")
        operatorChar = try! NSRegularExpression(pattern: "[\\+\\-\\*\\/dD]")
        workStack = Array<String>()
    }
    
    func identifyOperatorType(_ op: String) -> DiceParsingOperator{
        switch (op) {
        case "+",
             "-":
            return .ADD_SUBTRACT;
        case "*",
             "/":
            return .MULTIPLY_DIVIDE;
        case "d",
             "D":
            return .DICE;
        default:
            if (self.showWork) {
                let message = String(format: "Tried to identify unknown operator: %c", op)
                self.workStack.append(message)
            }
            return .UNKNOWN;
        }
    }
    
    func rollDice(_ numberOfDice: Int, withSides numberOfSides: Int) -> Int{
        
        var result = 0 //The total result of this dice expression
        var diceRolls = Array<String>(repeating: "", count: numberOfDice) //Storage for individual results
        
        for _ in 1...numberOfDice {
            let rollResult = Int(arc4random_uniform(UInt32(numberOfSides)) + 1)
            result += rollResult;
            diceRolls.append(String(format:"[%i]  ", rollResult))
        }
        
        //Generate the work string for this roll
        if (self.showDice) {
            var workString = ""
            
            //Add the individual rolls
            for rollResult in diceRolls as [String] {
                workString += rollResult
            }
            
            //Add the total
            let totalString = String(format:"  Total: %i", result)
            workString += totalString
            
            //Put it all together
            workString = String(format:"Dice Roll: %id%i : %@", numberOfDice, numberOfSides, workString)
            self.workStack.append(workString)
        }
        
        return result
    }
    
    func collapseExpression(firstOperand:Int, operation: String, secondOperand: Int) throws -> Int {
        
        switch (operation) {
        case "+":
            let result = firstOperand + secondOperand
            
            //Log the result
            if (self.showWork) {
                let workString = String(format:"Addition: %i + %i = %i", firstOperand, secondOperand, result)
                self.workStack.append(workString)
            }
            
            return result
            
        case "-":
            let result = firstOperand - secondOperand
            
            //Log the result
            if (self.showWork) {
                let workString = String(format: "Subtraction: %i - %i = %i", firstOperand, secondOperand, result)
                self.workStack.append(workString)
            }
            
            return result
            
        case "*":
            let result = firstOperand * secondOperand
            
            //Log the result
            if (self.showWork) {
                let workString = String(format: "Multiplication: %i * %i = %i", firstOperand, secondOperand, result)
                self.workStack.append(workString)
            }
            
            return result
            
        case "/":
            let result = firstOperand / secondOperand
            
            //Log the result
            if (self.showWork) {
                let workString = String(format: "Division: %i / %i = %i", firstOperand, secondOperand, result)
                self.workStack.append(workString)
            }
            
            return result
            
        case "d",
             "D":
            return rollDice(firstOperand, withSides: secondOperand)
            
        default:
            let message = String(format: "Attempted to collapse expression, encountered invalid operator: %@. First Operand: %i, Second Operand: %i", operation, firstOperand, secondOperand)
            throw StackCollapseError(message: message)
        }
    }
    
    func collapseStack(_ expressionStack: inout Array<String>) -> Int? {
        //Get the top element on the stack, which is an operand
        do{
            if(expressionStack.count < 3) {
                var message = "Attempted to collapse expression stack with less than 3 elements"
                message += String(format:"\nStack:\n%@", expressionStack)
                throw StackCollapseError(message: message)
            }
            
            let secondOperandString = expressionStack.popLast()!
            let operation = expressionStack.popLast()!
            let firstOperandString = expressionStack.popLast()!
            
            guard let secondOperand = Int(secondOperandString) else {
                let message = String(format: "Attempted to collpase expression stack, found non integer second operand: %@. Operation: %@, First Operand: %@", secondOperandString, operation, firstOperandString)
                throw StackCollapseError(message: message)
            }
            
            guard let firstOperand = Int(firstOperandString) else {
                let message = String(format: "Attempted to collpase expression stack, found non integer first operand: %@. Operation: %@, Second Operand: %@", firstOperandString, operation, secondOperandString)
                throw StackCollapseError(message: message)
            }
            
            let result = try collapseExpression(firstOperand: firstOperand, operation:operation, secondOperand:secondOperand)
            
            return result
        } catch {
            if(self.showWork) {
                let collapseError = error as! StackCollapseError
                self.workStack.append(collapseError.message)
            }
            
            return nil
        }
    }
    
    func canProceedWithCollapseOn(expression: String,
                                  fromIndex index:String.Index,
                                  withLastOperatorPrecedence lastOperator:DiceParsingOperator) -> Bool{
        
        let indexRange = index...
        let nsRange = NSRange(indexRange, in: expression)
        let nextOpLocation = operatorChar.rangeOfFirstMatch(in:expression, range:nsRange)
        if (nextOpLocation.location == NSNotFound) {
            return true;
        }
        let nextOp = String(expression[Range(nextOpLocation, in: expression)!])
        let nextOpPrecedence = identifyOperatorType(nextOp)
        if (lastOperator >= nextOpPrecedence) {
            return true;
        }
        
        return false;
    }
    
    func parse(expression: String) -> Int?{
        var expressionStack = Array<String>()
        var lastOperator = DiceParsingOperator.UNKNOWN;
        
        //Walk through the expression, looking for tokens
        var index = expression.startIndex
        while index < expression.endIndex {
            
            //Try to collapse the stack
            // If we have at least 3 items
            // And there are an odd number of items on the stack
            // And we can proceed because there are no higher order operations pending
            while (expressionStack.count >= 3 &&
                expressionStack.count % 2 == 1 &&
                canProceedWithCollapseOn(expression:expression,
                                         fromIndex:index,
                                         withLastOperatorPrecedence:lastOperator)) {
                                            
                                            let result = collapseStack(&expressionStack)!
                                            
                                            
                                            if(expressionStack.count >= 2){
                                                let lastOpToken = expressionStack.last!
                                                lastOperator = identifyOperatorType(String(lastOpToken.first!))
                                            }
                                            expressionStack.append(String(describing:result))
            }
            
            
            //Get the range for the next character, need an NSRange version for regex operations
            let nextCharRange = index..<expression.index(after: index)
            let nextCharNsRange = NSRange(nextCharRange, in: expression)
            
            //Get the range for the rest of the string, need an NSRange version for regex operations
            let remainingRange = index..<expression.endIndex
            let remainingNsRange = NSRange(remainingRange, in: expression)
            if(number.numberOfMatches(in:expression, range:nextCharNsRange) > 0){
                
                //Handle number tokens
                let tokenRange = number.rangeOfFirstMatch(in:expression, range:remainingNsRange)
                let stringRange = Range(tokenRange, in:expression)!
                let token = String(expression[stringRange])
                expressionStack.append(token)
                
                index = expression.index(index, offsetBy: tokenRange.length) // Set index to the next position after this token
                
            } else if (operatorChar.numberOfMatches(in:expression, range:nextCharNsRange) > 0){
                
                //Handle operator tokens
                let op = String(expression[index])
                
                if (expressionStack.count % 2 == 0) {
                    if (self.showWork) {
                        let message = String(format: "Encountered an operator token (%@) at an invalid location: skipping token.", op)
                        self.workStack.append(message)
                    }
                    index = expression.index(index, offsetBy: 1)
                    continue
                }
                
                lastOperator = identifyOperatorType(op)
                
                expressionStack.append(op)
                
                index = expression.index(index, offsetBy: 1) //Set index to the next position after this token
                
            } else if (openParenthesis.numberOfMatches(in:expression, range:nextCharNsRange) > 0){
                
                //Handle parenthesis
                var indexOfClose = index;
                var indexOfNextOpen : String.Index? = expression.index(after: index)
                repeat {
                    //Ensure we always increment to avoid infinite loops
                    indexOfClose = expression.index(after: indexOfClose)
                    indexOfNextOpen = expression.index(after: indexOfNextOpen!)
                    
                    //Get the index of the next close parenthisis
                    let remainder = indexOfClose..<expression.endIndex
                    let remainderNs = NSRange(remainder, in: expression)
                    let firstMatch = closeParenthesis.rangeOfFirstMatch(in:expression, range:remainderNs)
                    
                    if let matchRange = Range(firstMatch, in: expression) {
                        indexOfClose = matchRange.lowerBound
                    } else {
                        //If we found an open parenthisis with no close, that is an error
                        if (self.showWork) {
                            self.workStack.append("Error parsing parenthises, unable to find close parenthisis")
                        }
                        return nil;
                    }
                    
                    //Get the index of the next open parenthisis
                    let remainderFromNextOpen = indexOfNextOpen!..<expression.endIndex
                    let remainderFromNextOpenNs = NSRange(remainderFromNextOpen, in: expression)
                    let nextOpenMatch = openParenthesis.rangeOfFirstMatch(in:expression, range:remainderFromNextOpenNs)
                    
                    if let matchRange = Range(nextOpenMatch, in: expression) {
                        indexOfNextOpen = matchRange.lowerBound
                    } else {
                        indexOfNextOpen = nil
                    }
                    
                } while (indexOfNextOpen != nil && indexOfClose > indexOfNextOpen!);
                
                let subStringRange = expression.index(after: index)..<indexOfClose
                
                if (self.showWork) {
                    let message = String(format: "--- Begining Evaluation of Sub-Expression: \"%@\" ---", String(expression[subStringRange]))
                    self.workStack.append(message)
                }
                
                if let token = parse(expression: String(expression[subStringRange])){
                    
                    if (self.showWork) {
                        let message = String(format: "--- Ending Evaluation of Sub-Expression, Result: %i ---", token)
                        self.workStack.append(message)
                    }
                    
                    expressionStack.append(String(describing: token))
                } else {
                    if (self.showWork) {
                        let message = String(format: "Result from sub expression \"%@\" resulted in an error.", String(expression[subStringRange]))
                        self.workStack.append(message)
                    }
                    
                    return nil;
                }
                
                index = expression.index(indexOfClose, offsetBy:1) // Set index to the next position after this token
            } else {
                index = expression.index(after: index)
            }
            
            
        }
        
        while (expressionStack.count >= 3 && expressionStack.count % 2 == 1) {
            if let collapse = collapseStack(&expressionStack) {
                expressionStack.append(String(describing:collapse))
            } else {
                if(self.showWork){
                    workStack.append("Encountered an error collapsing the expression stack")
                }
            }
        }
        
        if (expressionStack.count != 1) {
            
            if (self.showWork) {
                self.workStack.append(String(format:"Reached the end of expression, exression stack still has items with size: %lu", expressionStack.count))
                self.workStack.append(String(format:"Stack is: %@", expressionStack))
                self.workStack.append(String(format:"Expression was: %@", expression))
            }
            
            return nil;
        }
        
        return Int(expressionStack.last!)
        
    }
    
    
    
    
    /**
     Evaluate a given dice expression
     
     An expression is a string that takes the form of
     
     ````
     expression     : operand
                    | operand operator operand
                    ;
     
     operand        : expression
                    | number
                    | die-expression
                    ;
     
     operator       : [+-/\*]
                    ;
 
     number         : [0-9]+
                    ;
 
     die-expression : number [dD] number
                    ;
     
     ````
     
     Expressions and sub-expressions can be wrapped in parenthesis. The order of operations for parsing the expression is as follows:
     
     1. expressions in parenthesis
     2. die-expression
     3. multiplication / division
     4. addition / subtraction
 
     - Parameters:
        - expression: the expression to parse
        - showDice: optional - whether or not to include the die roll results in the work stack
        - showWork: optional - whether or not to include the intermediate math steps in the work stack
     - Returns: a DiceExpressionResult with the results of the parsing and any requested work information
     */
    public func evaluate(_ expression:String, showDice: Bool = false, showWork: Bool = false) -> DiceExpressionResult {
        self.workStack = Array<String>()
        self.showDice = showDice;
        self.showWork = showWork;
        
        let result = self.parse(expression: expression)
        let expressionResult = DiceExpressionResult(result:result, workStack:workStack);
        
        return expressionResult;
    }
    
}

