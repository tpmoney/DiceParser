//
//  DiceParsingOperator.swift
//  DiceParser
//
//  Created by  on 2/26/17.
//  Copyright © 2017 Tevis Money. All rights reserved.
//  Licensed under the BSD 3-Clause License, see LICENSE file for details
//

import Foundation

public enum DiceParsingOperator : Int, Comparable {
    case UNKNOWN = -1
    case ADD_SUBTRACT
    case MULTIPLY_DIVIDE
    case DICE
    
    public static func < (a:DiceParsingOperator, b:DiceParsingOperator) -> Bool {
        return a.rawValue < b.rawValue
    }
}
