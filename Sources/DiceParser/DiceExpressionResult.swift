//
//  DiceExpressionResult.swift
//  DiceParser
//
//  Created by  on 2/26/17.
//  Copyright © 2017 Tevis Money. All rights reserved.
//  Licensed under the BSD 3-Clause License, see LICENSE file for details
//

import Foundation

public class DiceExpressionResult : Codable {
    
    public let workStack : Array<String>
    public let result : Int?
    
    init(result:Int?, workStack:Array<String>) {
        self.workStack = workStack
        self.result = result
    }
    
}
