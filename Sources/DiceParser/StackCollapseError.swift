//
//  StackCollapseError.swift
//  DiceParser
//
//  Created by  on 2/28/17.
//  Copyright © 2017 Tevis Money. All rights reserved.
//

import Foundation

class StackCollapseError : Error {
    
    let message : String
    
    init(message: String){
        self.message = message
    }
}
