import XCTest

import DiceParserTests

var tests = [XCTestCaseEntry]()
tests += DiceParserTests.allTests()
XCTMain(tests)