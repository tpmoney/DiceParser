//
//  DiceParserTests.swift
//  DiceParserTests
//
//  Created by  on 2/26/17.
//  Copyright © 2017 Tevis Money. All rights reserved.
//  Licensed under the BSD 3-Clause License, see LICENSE file for details
//

import XCTest
@testable import DiceParser

class DiceParserTests: XCTestCase {
    
    let parser = DiceExpressionParser()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        parser.showWork = false
        parser.showDice = false
    }
    
    func testAddition(){
        let expression = "1+1"
        let expressionResult = parser.evaluate(expression)
        
        XCTAssert(expressionResult.result == 2)
    }
    
    func testSubtraction(){
        let expression = "10-5"
        let expressionResult = parser.evaluate(expression)
        
        XCTAssert(expressionResult.result == 5)
    }
    
    func testMultiplication(){
        let expression = "2*3"
        let expressionResult = parser.evaluate(expression)
        
        XCTAssert(expressionResult.result == 6)
    }
    
    func testDivision(){
        let expression = "6/2"
        let expressionResult = parser.evaluate(expression)
        
        XCTAssert(expressionResult.result == 3)
    }
    
    func testDiceExpression(){
        let expression = "2d6"
        let result = parser.evaluate(expression)
        
        let range = 2...12
        
        XCTAssert(range ~= result.result!)
    }
    
    func testOrderOfOperations(){
        let expression = "3*(2 + 1)- 2/2"
        let result = parser.evaluate(expression)
        
        XCTAssert(result.result == 8)
    }
    
    func testReturnSelf(){
        let result = parser.evaluate("1")
        
        XCTAssert(result.result == 1)
    }
    
    func testDiceRollsInWorkStack(){
        let result = parser.evaluate("3d6", showDice: true)
        
        XCTAssertFalse(result.workStack.isEmpty)
        XCTAssert(result.workStack.count == 1)
    }
    
    func testNestedSubExpressions(){
        let expression = "(1 * (3+3)) / (5 - (1 *1) - 2 )"
        let result = parser.evaluate(expression, showWork: true)
        
        XCTAssert(result.result == 3)
    }
    
    func testShowWork(){
        let expression = "(1 * (3+3)) / (5 - (1 *1) - 2 )"
        let result = parser.evaluate(expression, showWork: true)
                
        XCTAssertFalse(result.workStack.isEmpty)
        XCTAssert(result.workStack.count == 14)
    }
    
    func testSubExpressionError(){
        let expression = "(1 * (4s5))"
        let result = parser.evaluate(expression, showWork: true)
        
        XCTAssert(result.result == nil)
        XCTAssert(result.workStack.last == "Result from sub expression \"1 * (4s5)\" resulted in an error.")
    }
    
    func testInvalidOperatorLocation(){
        let expression = "2 + * 3"
        let result = parser.evaluate(expression, showWork: true)
                
        XCTAssert(result.result == 5)
        XCTAssert(result.workStack.first == "Encountered an operator token (*) at an invalid location: skipping token.")
    }
    
    func testMissingOpenParenthesis(){
        let result = parser.evaluate("1 + (2 * (3 ) + 4", showWork: true)
        
        XCTAssert(result.result == nil)
        XCTAssert(result.workStack.first == "Error parsing parenthises, unable to find close parenthisis")
    }
    
    func testInvalidOperator(){
        
        XCTAssertThrowsError(try parser.collapseExpression(firstOperand: 1, operation: "q", secondOperand: 1))
        
    }
    
    func testExpressionStackTooSmall(){
        var stack = ["1", "+"]
        let value = parser.collapseStack(&stack)
        
        XCTAssert(value == nil)
    }
    
    func testNonIntegerSecondOperand(){
        var stack = ["1", "+", "Q"]
        let value = parser.collapseStack(&stack)
        
        XCTAssert(value == nil)
    }
    
    func testNonIntegerFirstOperand(){
        parser.showWork = true
        var stack = ["Q", "+", "1"]
        let value = parser.collapseStack(&stack)
        
        XCTAssert(value == nil)
        XCTAssert(parser.workStack.first!.hasPrefix("Attempted to collpase expression stack, found non integer first operand:"))
    }
    
    func testMalformedExpression(){
        let expression = "1 2 3 + 6"
        let result = parser.evaluate(expression, showWork:true)
                
        XCTAssert(result.result == nil)
        
    }
    
    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measure {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
